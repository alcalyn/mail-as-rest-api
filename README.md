# Mailer as a Rest Api

Send an email by `POST /mail` to this API.

## Install

Requires NodeJs and a SMTP server.

``` bash
git clone https://framagit.org/alcalyn/mail-as-rest-api.git
cd mail-as-rest-api/

cp .env .env.local
```

Edit your `.env.local`.

Then Run:

``` bash
yarn start
```

## Usage

Once running, you can use the API like:

``` rest
POST http://0.0.0.0:3000/mail
Content-Type: application/json

{
    "from": "someone@example.org",
    "subject": "Optional subject",
    "text": "Hello \n there",
    "html": "<p>Hello <br> there</p>"
}
```

But it allows data to be passed as json like the example above, but also as form data:

## Development

Run:

``` bash
yarn dev
```

to run server and watch for file changes to rebuild.

## License

This library is under [AGPL-3.0 license](LICENSE).
