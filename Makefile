.PHONY: all
all: up start

.PHONY: install
install: up node_modules

.PHONY: start
start:
	docker-compose exec node sh -c "yarn dev"

.PHONY: start-prod
start-prod:
	docker-compose exec node sh -c "yarn start"

.PHONY: stop
stop:
	docker-compose down

.PHONY: up
up:
	docker-compose up -d

.PHONY: logs
logs:
	docker-compose logs -ft

.PHONY: bash
bash:
	docker-compose exec node sh

node_modules:
	docker-compose exec node sh -c "apk add git && yarn"
