require('./config');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const multer = require('multer');
const upload = multer();

const postMailController = require('./postMailController');
const logger = require('./logger');

// Create app
const app = express();

// Handle any type of data submission, json, form or multipart
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(upload.none());

// Add cors
app.use(cors());

// Logger middleware
app.use((req, res, next) => {
    logger.info(`${req.method} ${req.url}`);
    next();
});

// Post mail endpoint
app.post('/mail', postMailController);

// Run server
const port = process.env.PORT;

app.listen(port, () => {
    logger.info(`Environment: ${process.env.NODE_ENV}`)
    logger.info(`Example app listening on port ${port}!`);
});
