const winston = require('winston');

if (process.env.NODE_ENV === 'production') {
    winston.add(new winston.transports.File({
        level: 'warn',
        filename: 'errors.log',
        format: winston.format.timestamp(),
    }));
} else {
    winston.add(new winston.transports.Console({
        format: winston.format.cli(),
    }));
}

module.exports = winston;
