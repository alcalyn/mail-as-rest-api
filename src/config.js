const fs = require('fs');
const dotenv = require('dotenv');

dotenv.config();

if (fs.existsSync('.env.local')) {
    const envConfig = dotenv.parse(fs.readFileSync('.env.local'));

    for (const key in envConfig) {
        process.env[key] = envConfig[key];
    }
}
