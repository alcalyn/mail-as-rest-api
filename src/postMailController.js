const mailer = require('./mailer');
const logger = require('./logger');

module.exports = (req, res, next) => {
    const mail = req.body;

    mailer
        .sendMail({
            from: mail.from,
            to: process.env.MAIL_TO,
            subject: mail.subject,
            text: mail.text,
        })
        .then(() => {
            logger.info('Mail sent successfully.');

            res.send({
                sent: true,
            });

            next();
        })
        .catch((e) => {
            logger.error('Error while trying to send email: ', e);

            res.send({
                sent: false,
            });

            next();
        })
    ;
};
